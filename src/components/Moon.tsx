// @ts-nocheck
'use client'
import React, {useEffect, useMemo, useRef, useState} from 'react'
import {Canvas, useFrame} from '@react-three/fiber'
import {
  AdaptiveDpr,
  CameraShake,
  Environment,
  Stars,
  // useDetectGPU,
  useTexture,
  Stats,
  ScrollControls
} from "@react-three/drei";
import MoonModel from "@/components/MoonModel";
import {EffectComposer, SelectiveBloom, Vignette} from "@react-three/postprocessing";
import {BlendFunction, KernelSize, Resolution} from "postprocessing";

export default function Moon() {
  const ambient = useRef()
  const model = useRef()
  const config = {
    maxYaw: 0.05, // Max amount camera can yaw in either direction
    maxPitch: 0.05, // Max amount camera can pitch in either direction
    maxRoll: 0.05, // Max amount camera can roll in either direction
    yawFrequency: 0.05, // Frequency of the the yaw rotation
    pitchFrequency: 0.05, // Frequency of the pitch rotation
    rollFrequency: 0.05, // Frequency of the roll rotation
    intensity: 1, // initial intensity of the shake
    decay: false, // should the intensity decay over time
    decayRate: 0, // if decay = true this is the rate at which intensity will reduce at
    controls: undefined, // if using orbit controls, pass a ref here so we can update the rotation
  }
  // const GPUTier = useDetectGPU()

  return (
      <Canvas camera={{position: [0, 0, -8], fov: 50}} style={{position: 'fixed', top: 0, left: 0}}>
        <ambientLight ref={ambient}/>
        <pointLight position={[10, 10, 10]}/>
        <MoonModel position={[0, 0, 0]}/>
        <Stars radius={100} depth={50} count={5000} factor={4} saturation={0} fade speed={1}/>
        {/*{*/}
        {/*    GPUTier.fps > 60 &&*/}
        {/*    <CameraShake {...config} />*/}
        {/*}*/}
        <Stats showPanel={0} className="stats"/>

        <EffectComposer>
          {
              // GPUTier.fps > 60 &&
              <>
                  <SelectiveBloom
                      // selection={[model]}
                      lights={ambient}
                      luminanceThreshold={0}
                      luminanceSmoothing={0.3}
                      width={Resolution.AUTO_SIZE}
                      height={Resolution.AUTO_SIZE}
                      kernelSize={KernelSize.VERY_SMALL}
                      blendFunction={BlendFunction.SCREEN}
                  />
              </>
          }
        </EffectComposer>
        <AdaptiveDpr pixelated/>
      </Canvas>
  )
}

