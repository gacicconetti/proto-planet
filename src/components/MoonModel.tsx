// @ts-nocheck
'use client'
import React, {useEffect, useMemo, useRef, useState} from 'react'
import {Canvas, extend, useFrame} from '@react-three/fiber'
import {Environment, shaderMaterial, Stars, useTexture} from "@react-three/drei";
import {Color} from "three";
import glsl from "babel-plugin-glsl/macro"
import {useSpring, useTransform, motion, useScroll} from "framer-motion";
import useWindowDimensions from "@/hooks/useWindowDimensions";

extend({
  PlanetMaterial: shaderMaterial(
      {
        uTime: 0,
        uDisplace: 5.0,
        uIntensity: 10.0,
        uColorStart: new Color('#FF69B4'),
        uColorEnd: new Color('#f8e1fc'),
      },
      glsl`
        varying vec3 vPos;
        varying vec3 vNormal;
        varying vec2 vUv;
        uniform vec3 uColorStart;
        void main() {
          vPos = (modelMatrix * vec4(position, 1.0)).xyz;
          vNormal = normalMatrix * normal;
          vUv = uv;
          gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
        }`,
      glsl`
        uniform vec3 diffuse;
        varying vec3 vPos;
        varying vec3 vNormal;
        #pragma glslify: cnoise3 = require(glsl-noise/classic/3d.glsl) 
        #define PI 3.14159265358979323846
        uniform float uTime;
        uniform vec3 uColorStart;
        uniform vec3 uColorEnd;
        uniform float uPlanet;
        uniform float uDisplace;
        uniform float uIntensity;
        uniform float density;
        varying vec2 vUv;
        uniform float x;
        uniform float y;
        vec2 displacedUv;
        float strength;
        float outerGlow;
        void main() {
          displacedUv = vUv + cnoise3(vec3(vUv * (uIntensity), (uTime) * 0.1));
          strength = cnoise3(vec3(displacedUv * uDisplace, uTime * 0.1)) * 50.0;
          outerGlow = distance(vUv, vec2(0.5)) * 20.0;
          strength += outerGlow * 0.0;
          strength += step(1.0, strength);
          strength = clamp(strength, 0.0, 1.0);
          vec3 color = mix(uColorStart, uColorEnd, strength);
              
          vec4 addedLights = vec4(.0, .0, .0, 1.0);
          vec3 adjustedLight = vec3(-1.0);
          vec3 lightDirection = normalize(adjustedLight + vec3(0.0,cameraPosition.y,0.0));
          addedLights.rgb += clamp(dot(-lightDirection, vNormal), 0.0, 1.0) * color;
          gl_FragColor = mix(vec4(color, 1.0), addedLights, 0.98);
        }
        `,
      (material) => {
      }
  ),
})
export default function MoonModel(props: any) {
  const scroll = useScroll()
  const sp = useSpring(scroll.scrollY, {damping: 50})
  const ringMaterial = useRef()
  const mesh = useRef()
  const backgroundColorStart = useTransform(
      sp,
      [0, 3000],
      ["#FF69B4", "#43a8fa"]
  )
  const backgroundColorEnd = useTransform(
      sp,
      [0, 3000],
      ["#f8e1fc", "#92caf7"]
  )
  const uDisplace = useTransform(
      sp,
      [0, 8000],
      [5.0, 30.0]
  )
  const uIntensity = useTransform(
      sp,
      [0, 3000],
      [10.0, 50.0]
  )
  useFrame((state, delta) => {
    mesh.current.rotation.z += delta / 10
    if (ringMaterial && ringMaterial.current) {
      ringMaterial.current['uTime'] += delta
      ringMaterial.current['x'] = state.mouse.x
      ringMaterial.current['y'] = state.mouse.y
      ringMaterial.current['uDisplace'] = uDisplace.get()
      ringMaterial.current['uIntensity'] = uIntensity.get()
      ringMaterial.current['uColorStart'] = new Color(backgroundColorStart.get())
      ringMaterial.current['uColorEnd'] = new Color(backgroundColorEnd.get())
    }
    state.camera.rotation.y = Math.sin(sp.get() / 600) / -2
    state.camera.rotation.z = Math.sin(sp.get() / 600) / -5
    console.log(Math.sin(sp.get() / 600) / -2)
  })
  return (
      <mesh
          {...props}
          ref={mesh}
      >
        <sphereBufferGeometry args={[2.5, 64, 64]}/>
        {/*<meshStandardMaterial/>*/}
        <planetMaterial ref={ringMaterial}/>
      </mesh>
  )
}
