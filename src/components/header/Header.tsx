'use client'
import styles from './header.module.css'
import {useMotionValueEvent, useScroll, motion, useTransform, useSpring} from "framer-motion";
import useWindowDimensions from "@/hooks/useWindowDimensions";
import React from "react";
import Logo from "@/components/logo/logo";

export default function Header() {
  const wd = useWindowDimensions()
  const {scrollY} = useScroll();
  const transf = useTransform(scrollY, [0, wd.height - 60], [0, -20])
  const transf_radius = useTransform(scrollY, [0, wd.height - 60], [70, 0])
  const transf_w = useTransform(scrollY, [0, wd.height - 60], [60, 600])
  const transf_w_spring = useSpring(transf_w, {stiffness: 500, damping: 50})

  return (
      <motion.header className={styles.header} style={{translateY: transf}}>
        <motion.div className={styles.inner} style={{
          // borderRadius: transf_radius,
          width: transf_w_spring
        }}>
          <Logo/>
        </motion.div>
      </motion.header>
  );
}
